import socket
import serial
import cv2
import struct
import binascii
from threading import Thread


def int2bytes(i):
    hex_string = '%x' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))


cap=cv2.VideoCapture(0)
arduino = serial.Serial('COM3',115200,timeout= .1)
TCP_IP = '127.0.0.1' # deneme localhost
TCP_PORT = 8001 #port
BUFFER_SIZE = 1024 #gelecek byte
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# socket
s.bind((TCP_IP, TCP_PORT))
s.listen(1) #socket dinlenmeye
conn, addr = s.accept() # gelen  kabul ediliyor

print 'Baglanan adres:', addr

def connectionPart():
    while 1:
        data = conn.recv(BUFFER_SIZE)
        try:
             if (data == None):
                 break

             b = bytearray()
             b.extend(map(ord, data))
             das = str(b.decode("utf-16"))
             asd = das.split('/')
             if (len(asd) != 5):
                 continue
             print das
             arduino.write(das)

        except ValueError:

            print 'err ', ValueError

def cameraSensor():
    while 1:
        ret, frame = cap.read()
        cv2.imshow('window-name', frame)
        data = frame.data

        sensor = arduino.readline().rstrip('\n')
        sensor = sensor.rstrip('\r')

        imageData = bytearray(struct.pack("L", len(data))) + data
        sensorData = struct.pack("L", len(sensor)) + sensor
        conn.send(imageData + sensorData)


if __name__ == "__main__":
    thread1 = Thread(cameraSensor())
    thread2 = Thread(connectionPart())
    thread1.start()
    thread2.start()


conn.close()
